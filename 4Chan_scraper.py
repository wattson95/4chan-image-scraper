# -*- coding: utf-8 -*-
from bs4 import BeautifulSoup
import urllib2, json, urllib, time, os, errno, re, string, sys

BASE_THREAD_URL = ''
BASE_IMAGE_URL = ''
THREADS_URL = ''
WIDTH = ''
HEIGHT = ''
EXCLUDE = ''
BOARD = ''
BOARD_LIST = ['a','b','c','d','e','f','g','gif','h','hr','k','m','o','p','r','s','t','u','v','vg','vr','w','wg','i','ic','r9k','s4s','cm','hm','lgbt','y','3','aco','adv','an','asp','biz','cgl','ck','co','diy','fa','fit','gd','hc','his','int','jp','lit','mlp','mu','n','news','out','po','pol','qst','sci','soc','sp','tg','toy','trv','tv','vp','wsg','wsr','x']
SPIN = 0
# 0 = |
# 1 = /
# 2 = -
# 3 = \
# Example comment

def set_variables (width, height, board, exclude):
	global WIDTH
	global HEIGHT
	global BASE_THREAD_URL
	global BASE_IMAGE_URL
	global THREADS_URL
	global WIDTH
	global HEIGHT
	global EXCLUDE
	global BOARD

	if (width == 'all'):
		WIDTH = True
	elif (width.isdigit()):
		WIDTH = width
	else:
		WIDTH = "Please enter a positive numeric width value"

	if (height == 'all'):
		HEIGHT = True
	elif(height.isdigit()):
		HEIGHT = height
	else:
		HEIGHT = "Please enter a positive numeric height value"


	for i in BOARD_LIST:
		if (i == board):
			BOARD = board

	if (BOARD == ''):
		BOARD = "Please enter a valid board"
	else:
		BASE_THREAD_URL = ("http://a.4cdn.org/%s/thread/" % BOARD) 
		BASE_IMAGE_URL = ("http://i.4cdn.org/%s/" % BOARD) 
		THREADS_URL = ("http://a.4cdn.org/%s/threads.json" % BOARD) 

	EXCLUDE = exclude

	if (EXCLUDE != ''):
		EXCLUDE = EXCLUDE.lower()
		EXCLUDE = EXCLUDE.replace(" ", "")
		EXCLUDE = EXCLUDE.split(",")

def check_exclusion (str1, str2):
	op = str1 + str2
	op = re.sub('[%s]' % re.escape(string.punctuation), '', op)
	op = op.replace(" ", "")
	op = op.lower()
	for i in EXCLUDE:
		if i in op:
			return False
	return True

def spinner ():
	global SPIN
	if (SPIN > 3):
		SPIN = 0

	if (SPIN == 0):
		sys.stdout.write('\rWorking |')
		sys.stdout.flush()
	elif (SPIN == 1):
		sys.stdout.write('\rWorking /')
		sys.stdout.flush()
	elif (SPIN == 2):
		sys.stdout.write('\rWorking -')
		sys.stdout.flush()
	elif (SPIN == 3):
		sys.stdout.write('\rWorking \\')
		sys.stdout.flush()

	SPIN = SPIN + 1

def get_images(thread):
	html = urllib2.urlopen(BASE_THREAD_URL + thread + ".json").read()
	count = 0
	jsonDoc = json.loads(html)
	
	folderName = ''
	
	try:
		folderName = unicode(jsonDoc['posts'][0]['sub']).encode('utf-8')
	except KeyError:
		folderName = unicode(jsonDoc['posts'][0]['com']).encode('utf-8')

	try:
		checkedName = check_exclusion (folderName, unicode(jsonDoc['posts'][0]['com']).encode('utf-8'))
	except KeyError:
		checkedName = check_exclusion (folderName, folderName)

	folderName = folderName[:25]
	folderName = re.sub('[%s]' % re.escape(string.punctuation), '_', folderName)

	if (checkedName):
		for i in jsonDoc['posts']:
			try:
				#print i
				if ((jsonDoc['posts'][count]['w'] == int(WIDTH) or type(WIDTH) is bool) and (jsonDoc['posts'][count]['h'] == int(HEIGHT) or type(HEIGHT) is bool)):
					fileName = os.environ['HOME'] + ("/Pictures/%s/" % BOARD) + folderName + "/img" + str(jsonDoc['posts'][count]['tim']) + str(jsonDoc['posts'][count]['ext'])
					folderPath = os.environ['HOME'] + ("/Pictures/%s/" % BOARD) + folderName
					sys.stdout.write('\r' + fileName + '\n')
					sys.stdout.flush()
					make_sure_path_exists(folderPath)
					urllib.urlretrieve(BASE_IMAGE_URL + str(jsonDoc['posts'][count]['tim']) + str(jsonDoc['posts'][count]['ext']), fileName)
					time.sleep(1)
					#count = count + 1
			except KeyError as ex:
				#print thread
				#print jsonDoc['posts'][count]['no']
				#count = count + 1
				time.sleep(1)
			except ValueError:
				print ""
				print "An invalid width or height was entered"
				print "Now exiting"
				exit()
			count = count + 1
			spinner()
	return None

def get_catalog():
	try:
		html = urllib2.urlopen(THREADS_URL).read()
	except ValueError:
		print ""
		print "An invalid board was entered"
		print "Please use one of the following"
		print ""
		print "[a,b,c,d,e,f,g,gif,h,hr,k,m,o,p,r,s,t,u,v,vg,vr,w,wg,i,ic,r9k,s4s,cm,hm,lgbt,y,3,aco,adv,an,asp,biz,cgl,ck,co,diy,fa,fit,gd,hc,his,int,jp,lit,mlp,mu,n,news,out,po,pol,qst,sci,soc,sp,tg,toy,trv,tv,vp,wsg,wsr,x]"
		print ""
		print "Now exiting"
		exit()

	#print html

	jsonDoc = json.loads(html)
	record = []
	i_count = 0
	for i in jsonDoc:
		#j_count = 0;
		#for j in jsonDoc[i_count]['threads']:
		record.append(jsonDoc[i_count]['threads'])
		i_count = i_count + 1
	#print jsonDoc
	return record

def make_sure_path_exists(path):
	try:
		os.makedirs(path)
	except OSError as exception:
		if exception.errno != errno.EEXIST:
			raise

def get_variables():
	width = raw_input("Please enter a width value in pixels (1920, 1600, etc), or all:")
	height = raw_input("Please enter a height value in pixels (1080, 900, etc), or all:")
	board = raw_input("Please enter a board to pull images from (wg for walpapers/general, b for random, etc:")
	exclude = raw_input("Please enter a csv of keywords, if a thread contains one in it's OP it'll be skipped (nsfw, desktop, nature, etc), or nothing for no filter:")
	set_variables(width, height, board, exclude)

if __name__ == '__main__':
	answer = ''
	while (answer == '' or answer.lower() == 'n'):
		get_variables()
		print ""
		
		if (type(WIDTH) is bool):
			print "Width: all"
		else:
			print "Width:", WIDTH
		
		if (type(HEIGHT) is bool):
			print "Height: all"
		else:
			print "Height:", HEIGHT
		
		print "Board:", BOARD
		print "Exclusions:", EXCLUDE
		print ""
		answer = raw_input("Are the above the correct details? (y/n):")

	catalog = get_catalog()

	count = 0
	for i in catalog:
		for j in catalog[count]:
			get_images(str(j['no']))
		count = count + 1